package helperToken

import (
	"time"

	"github.com/golang-jwt/jwt"
)

var (
	AUD            = "Pengguna"
	ISS            = "Pengguna.ISS"
	LOGIN_SECRET   = "Ini rahasia ya"
	MESSAGE        = "Unathorized!"
	MESSAGE_KOSONG = "Requst authorize kosong"
	WAKTU          = 30
)

func BuatJWT(iss, aud, secret string, exp time.Time) (string, error) {
	// generate jwt
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"Issuer":    iss,
		"Audience":  aud,
		"ExpiresAt": exp.Unix(),
	})

	// Sign and get the complete encoded token as a string using the secret
	// membuat jwtnya
	tokenString, err := token.SignedString([]byte(secret))
	return tokenString, err
}
