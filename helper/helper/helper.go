package helper

import (
	"encoding/json"
	"randi_firmansyah/models/productModel"
	"randi_firmansyah/models/userModel"
	"strings"
	"time"

	"golang.org/x/crypto/bcrypt"
)

func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

func UnMarshall(from string, to interface{}) error {
	if err := json.Unmarshal([]byte(from), &to); err != nil {
		return err
	}
	return nil
}

func SearchProduct(models []productModel.Product, id int) (interface{}, bool) {
	var oneData productModel.Product
	for i := 0; i < len(models); i++ {
		if models[i].Id == id {
			oneData = models[i]
			return oneData, true
		}
	}
	return nil, false
}

func SearchUser(models []userModel.User, id int) (interface{}, bool) {
	var oneData userModel.User
	for i := 0; i < len(models); i++ {
		if models[i].Id == id {
			oneData = models[i]
			return oneData, true
		}
	}
	return nil, false
}

func ExtractTokens(token string) string {
	/// Bearer asdhaskdkasdhsagdhasdgjasgdhasdghadgjadsaj
	strarr := strings.Split(token, " ")
	if len(strarr) == 2 {
		return strarr[1]
	}
	return ""
}

func ExpiredTime(menit int) time.Time {
	return time.Now().Add(time.Duration(menit) * time.Minute) // expired date
}
