package main

import (
	"log"
	"net/http"
	"randi_firmansyah/controllers/loginController"
	product "randi_firmansyah/controllers/productController"
	"randi_firmansyah/controllers/tokenController"
	user "randi_firmansyah/controllers/userController"

	"github.com/go-chi/chi"
)

func main() {
	r := chi.NewRouter()

	r.Group(func(g chi.Router) {
		g.Get("/globaltoken", loginController.GenerateTokens)
	})

	r.Group(func(l chi.Router) {
		l.Post("/login", loginController.Login)
	})

	r.Group(func(p chi.Router) {
		p.Use(tokenController.GetToken)
		p.Get("/product", product.GetSemuaProduct)
		p.Get("/product/{id}", product.GetProductById)
		p.Post("/product", product.PostProduct)
		p.Put("/product/{id}", product.UpdateProduct)
		p.Delete("/product/{id}", product.DeleteProduct)
	})

	r.Group(func(u chi.Router) {
		u.Use(tokenController.GetToken)
		u.Get("/user", user.GetSemuaUser)
		u.Get("/user/{id}", user.GetUserById)
		u.Post("/user", user.PostUser)
		u.Put("/user/{id}", user.UpdateUser)
		u.Delete("/user/{id}", user.DeleteUser)
	})

	log.Println("Running Service")

	if err := http.ListenAndServe(":5000", r); err != nil {
		log.Println("Error Starting Service")
	}
}
